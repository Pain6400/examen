import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ImagenesComponent } from './imagenes/imagenes.component';

@NgModule({
  declarations: [
    AppComponent,
    ImagenesComponent,
  ],
  imports: [
    BrowserModule,FormsModule,HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
