import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Hola mundo';
  centigrados : number = 25;

  kelvin () :number {
    return this.centigrados + 273;
  }

  farenheit() :number {
    return 9/5 * this.centigrados + 32;
  }

  verResultados() {
    window.alert('La temperatura es de ' + this.centigrados + ' celsius.');
  }
}
