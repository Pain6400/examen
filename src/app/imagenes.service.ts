import { Injectable } from '@angular/core';
import { Imagenes } from './Imagen';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ImagenesServices {

    constructor(private http: HttpClient) {
    }

    postImagen(Imagenes: Imagenes) {
        return this.http.post('https://MiAlbum.com/api/Fotos', Imagenes);
    }
}