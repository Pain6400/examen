import { Component, OnInit } from '@angular/core';
import {Imagenes} from '../imagen';
import { ImagenesServices } from '../imagenes.service';

@Component({
  selector: 'app-imagenes',
  templateUrl: './imagenes.component.html',
  styleUrls: ['./imagenes.component.css']
})
export class ImagenesComponent implements OnInit {
  title : string = "Imagenes";
  NuevaImagen : Imagenes;
  Imagenes: Imagenes[] = [];
  lista = ['perrin1', 'perrin2', 'perrin3'];
  constructor(private servicioImagenes: ImagenesServices) {
    this.NuevaImagen = new Imagenes(
      '','');
   }

  ngOnInit() {
    this.Imagenes.push(new Imagenes('Gato1','https://www.royalcanin.es/wp-content/uploads/2019/02/consejosadiestramientogatitoscabecera.png'));
    this.Imagenes.push(new Imagenes('Gato1','https://www.purina.es/gato/purina-one/sites/g/files/mcldtz1856/files/2018-06/6_El_comportamiento_de_los_gatitos%20%282%29.jpg'));
  }

  guardar (){
    this.servicioImagenes.postImagen(this.NuevaImagen).subscribe(
      (res)=>{
        this.Imagenes.unshift(this.NuevaImagen);
        this.NuevaImagen=new Imagenes(
          '','');
          window.alert("exitoooo");
        
      },
      (error)=>{
        console.log("error con agregar");
      }
    )
  }

}
