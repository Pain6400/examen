export class Imagenes {
    
    NombreImagen : string;
    Ruta: string;

    constructor (
        NombreImagen: string,
        Ruta: string) {
        this.NombreImagen = NombreImagen;
        this.Ruta = Ruta;
    }
}